/**
  ******************************************************************************
  * @file    tf_matrix_data_params.h
  * @author  AST Embedded Analytics Research Platform
  * @date    Sun Sep 11 00:14:06 2022
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  ******************************************************************************
  */

#ifndef TF_MATRIX_DATA_PARAMS_H
#define TF_MATRIX_DATA_PARAMS_H
#pragma once

#include "ai_platform.h"

/*
#define AI_TF_MATRIX_DATA_WEIGHTS_PARAMS \
  (AI_HANDLE_PTR(&ai_tf_matrix_data_weights_params[1]))
*/

#define AI_TF_MATRIX_DATA_CONFIG               (NULL)


#define AI_TF_MATRIX_DATA_ACTIVATIONS_SIZES \
  { 400, }
#define AI_TF_MATRIX_DATA_ACTIVATIONS_SIZE     (400)
#define AI_TF_MATRIX_DATA_ACTIVATIONS_COUNT    (1)
#define AI_TF_MATRIX_DATA_ACTIVATION_1_SIZE    (400)



#define AI_TF_MATRIX_DATA_WEIGHTS_SIZES \
  { 10512, }
#define AI_TF_MATRIX_DATA_WEIGHTS_SIZE         (10512)
#define AI_TF_MATRIX_DATA_WEIGHTS_COUNT        (1)
#define AI_TF_MATRIX_DATA_WEIGHT_1_SIZE        (10512)



#define AI_TF_MATRIX_DATA_ACTIVATIONS_TABLE_GET() \
  (&g_tf_matrix_activations_table[1])

extern ai_handle g_tf_matrix_activations_table[1 + 2];



#define AI_TF_MATRIX_DATA_WEIGHTS_TABLE_GET() \
  (&g_tf_matrix_weights_table[1])

extern ai_handle g_tf_matrix_weights_table[1 + 2];


#endif    /* TF_MATRIX_DATA_PARAMS_H */
